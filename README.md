# Fallout 4 Character Generator #
## Read Me ##

### What is this project? ###

Sometimes, just starting the game isn't enough. Character creation can take hours, and even then, there's questions about how you will play the game, what weapons to use, which faction to choose.
The Fallout 4 Character Generator (F4CG) will help you solve all those problems!

This project was inspired by /u/espnatual in [this thread](https://redd.it/6qgy5w).
See full credits below.

### Why should I use this? ###

Honestly, no clue. Some people might find it useful, or a starting point for ideas. Some people might find the most extreme build they can do, or explore avenues they haven't tried.

### How does it work? ###

That's the easy part. For the simplest, fastest use of this tool:

* Click "Randomize Build".

Presto. You now have a completely random character to go have fun with. If you want to edit more, and help design the character, then that's also easy:

* Stuff here.

### But WHY is this coded in HTML? ###

(Alternatively, why can I only use this in my browser?)
This project is almost entirely front-end, "click some stuff" coding, with very little "behind the scenes" logic.
Keeping it in webpage form enforces that design. Additionally, having it be HTML/CSS ensures it can be used on virtually any modern device.

### Contribution Guidelines ###

Right now, all contributions are considered. Pull Requests can get merged in, 

### Credits ###

* Odiefrom (Developer)
* /u/espnatual (Original Inspiration)